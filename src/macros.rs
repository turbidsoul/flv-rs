#[macro_export]
macro_rules! hex {
    ($expr:expr) => {{
        let mut s = String::new();
        for it in $expr {
            s.push_str(format!("{:02x} ", it).as_str());
        }
        if s.len() > 0 {
            s = String::from(&s[..s.len()-1]);
        }

        s
    }};
}