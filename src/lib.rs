mod core;
mod io;
mod macros;

pub use crate::core::*;

#[cfg(test)]
mod tests {
    use log::info;
    use simple_logger::SimpleLogger;

    use crate::core::FLV;
    #[test]
    fn test_header() {
        SimpleLogger::new().init().unwrap();
        info!("test flv header");
        let flv_result = FLV::open(r"d:\download\205877_20141122091515689_12_1280x720_800.flv");
        assert!(flv_result.is_ok(), "{}", flv_result.unwrap_err());
        let flv = flv_result.unwrap();
        assert_eq!(flv.header.signature, "FLV".to_string());
        assert!(flv.header.has_video());
        assert!(flv.header.has_audio());
        assert_eq!(flv.header.version, 1);
        assert_eq!(flv.header.size, 9);
    }

    #[test]
    fn test_write2file() {
        // SimpleLogger::new().init().unwrap();
        info!("test file read and write");
        let flv_result = FLV::open(r"d:\download\205877_20141122091515689_12_1280x720_800.flv");
        assert!(flv_result.is_ok(), "{}", flv_result.unwrap_err());
        let mut flv = flv_result.unwrap();
        flv.write_file("d:/test_flv3.flv").unwrap();
    }
}
