pub trait ConvertBytes {
    type T;

    fn to_bytes(&self) -> Vec<Self::T>;
}